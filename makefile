SHELL := /bin/bash

PWD=$(shell pwd)
STATUS=0

all: build copy-iframe-lib

init:
	./init.sh

build: init
	make -f pub-make -k build

build-with-basic-infra: init clone-basic-infra build
	echo "build with basic infra"

clone-basic-infra: init wget-orgs clone-exporters clone-basic-themes
	echo "infrastructure put in place"

clone-ere-infra: init clone-basic-infra clone-ere-repos
	echo "exp infrastructure put in place"

clone-basic-themes: init
	make -f pub-make -k clone-basic-themes

clone-ere-repos: init
	make -f pub-make -k clone-ere-repos

clone-exporters: init
	make -f pub-make -k clone-exporters

wget-orgs: init
	make -f pub-make -k wget-orgs

clean:
	make -f pub-make clean
	(rm -rf build; rm -rf ./ere-publisher)

clean-infra:
	make -f pub-make clean-infra

copy-iframe-lib:
	rsync -a src/runtime/iframe-resize/* build/code/runtime/iframe-resize/

server:
	make -k all; cd build/code/runtime; python -m SimpleHTTPServer
