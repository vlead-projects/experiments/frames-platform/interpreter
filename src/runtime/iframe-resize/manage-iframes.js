
var createNewIframe = function(name, id, width, height) {
  var newIframe = document.createElement("iframe");
  newIframe.setAttribute("width", width);
  newIframe.setAttribute("height", height);
  newIframe.setAttribute("frameBorder", "0");
  newIframe.name = name;
  newIframe.id = id;
  return newIframe;
};

var createContentIframe = function() {
  return createNewIframe("contentIframe", "contentIframe", "100%", "4500em");
};

var createFooterIframe = function() {
  return createNewIframe("footerIframe", "footerIframe", "100%", "155em");
};

var insertFrameInDivision = function(iframe, divId) {
  var divElem = parent.document.getElementById(divId);
  divElem.appendChild(iframe);
};

var deleteFrameInDivision = function(frameId, divId) {
  if (parent.document.getElementById(divId).hasChildNodes()) {
    var frameElem = parent.document.getElementById(frameId);
    if (frameElem !== null) {
      frameElem.parentNode.removeChild(frameElem);
    }
  }
};

var insertContentIframe = function() {
  var newFrame = createContentIframe();
  var division = document.getElementById("contentDiv");
  insertFrameInDivision(newFrame, division.id);
};

var insertFooterIframe = function() {
  var newFrame = createFooterIframe();
  var division = document.getElementById("footerDiv");
  insertFrameInDivision(newFrame, division.id);
};

var setFooterUrlToFooterIframe = function() {
  var iframe = document.getElementById("footerIframe");
  iframe.src = footerUrl;
};

var setSourceToContentIframe = function(htmlFile) {
  var cIframe = document.getElementById("contentIframe");
  cIframe.src = htmlFile;
};

var setContentToContentIframe = function(content) {
  var cIframe = document.getElementById("contentIframe");
  var contentDoc = cIframe.contentDocument;
  var elem = contentDoc.getElementById("contentDiv");
  elem.innerHTML = content;
};

var setContent = function(content, htmlFile) {
  setSourceToContentIframe(htmlFile);
  setTimeout(setContentToContentIframe, 100, content);
};

var setUsageInContentIframe = function() {
  var iframe = parent.document.getElementById("contentIframe");
  iframe.src = "https://info.vlabs.ac.in/analytics/analytics/usage.html";
};

var setUrlToSandBoxedContentIframe = function(url) {
  var iframe = document.getElementById("contentIframe");                                       
  iframe.sandbox = 'allow-scripts allow-same-origin allow-popups allow-forms';
  iframe.src = url;
};
