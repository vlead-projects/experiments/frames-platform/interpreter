#+TITLE: Styles for the Landing page
#+AUTHOR: VLEAD
#+DATE: [2019-06-10 Mon]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
Styles for the landing page.

* Styles
#+NAME: main
#+BEGIN_SRC css
.nav {
    background-color: #424949;
    padding: 5px;
    padding-left: 8px;
    padding-right: 8px;
    color: white;
    font-size: 10px;
    float: left;
    text-align: center;
}

#container {
    width: 100%;
    height: 100vh;
    margin: 0 0;
    position: relative;
}

#main, #rightmain, #splitter {
    position: absolute;
    top: 0;
    height: 100%;
}

#main {
    left: 0;
    width: 39%;
    background-color: #ccffcc;
    height: 100%;
    overflow: scroll;
}

#rightmain {
    right: 0;
    width: 60%;
    height: 95%;
}

#splitter {
    left: 39%;
    width: 1%;
    background-color: gray;
    cursor: move;
}

#main div, #rightmain div {
    position: absolute;
    top: 40%;
    left: 0;
    width: 100%;
    text-align: center;
}

#+END_SRC

#+NAME: narrative-styles
#+BEGIN_SRC css

      *,
      *:before,
      *:after {
      box-model: border-box;
      }
      
      a {
      float: right;
      margin-left: 10px;
      }

      .clickable{
        cursor: pointer;
      }

      .clickable:hover{
        outline: #4CAF50 solid 2px;
      }

      .para{
        color: blue;
      }

#+END_SRC

* Tangle
#+BEGIN_SRC css :tangle main.css :eval no :noweb yes
<<main>>
#+END_SRC

#+BEGIN_SRC css :tangle narrative-styles.css :eval no :noweb yes
<<narrative-styles>>
#+END_SRC

